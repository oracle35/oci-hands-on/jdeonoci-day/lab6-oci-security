# lab6-oci-security

Configure OCI Security services like Cloud Guard, Vulnerability Scanning, Threat Intelligence to secure your OCI resources

## Getting started

The objective of this lab is to secure our OCI tenant with the OCI security services like **Vulnerability Scanning**, **Cloud Guard**,  ..

## target architecture



## lab steps

### Task 1. Enable Vulnerability Scanning Service

1. Sign in to OCI tenancy; from the Oracle CLoud Console home page, click the **Navigation** Menu Button in the upper-left corner and hover over **Identity & Security** and select **Policies**.

    * Name: ***pol-vulnerabilityscanning***
    * Description: ***Policy to allow Vulnerability Scanning Service***
    * Compartment: ***root***
    * Policy Builder: ***Show Manual Editor***:

            allow service vulnerability-scanning-service to manage instances in tenancy
            allow service vulnerability-scanning-service to read compartments in tenancy
            allow service vulnerability-scanning-service to read vnics in tenancy
            allow service vulnerability-scanning-service to read vnic-attachments in tenancy

    ![elbs-menu](./images/sec-vss-policy.png)

    * Click **Create**

2. Create a new policy to allow your group access to Vulnerability Scanning Service resources:

    * Name: ***pol-vulnerabilityscanning-admin***
    * Description: ***Policy to allow access Vulnerability Scanning Resources***
    * Compartment: ***root***
    * Policy Builder: ***Show Manual Editor***:

            allow group <mygroup> to manage vss-family in tenancy


3. Click the **Navigation** Menu Button in the upper-left corner and hover over **Identity & Security** and select **Scanning**.

    ![elbs-menu](./images/sec-vss-policy-admin.png)

    * Click **Create Scan Recipe**
        * Type: ***Compute***
        * Name: ***Compute Recipe***
        * Compartment: ***your JDE Trial compartment***

      ![OCI VSS create recipe](./images/vss-create-compute-recipe.png)

3. Click the **Navigation** Menu Button in the upper-left corner and hover over **Identity & Security** and select **Scanning**.

    * Click **Create**
        * Type: ***Compute***
        * Name: ***target-jdetrial***
        * Compartment: ***your JDE Trial compartment***
        * Description: ***JDE Trial Instance***
        * Scan Recipe: ***Compute Recipe***
        * Target Compartment: ***your JDE Trial compartment***
        * Targets: ***Select the JDE Trial instance***

      ![OCI VSS create target](./images/vss-create-compute-target.png)

After a few minutes (about 15 mn), you can click **View Scan Results** to check the ports scanning results

  ![OCI VSS results button](./images/vss-scan-results-button.png)

  ![OCI VSS results button](./images/vss-ports-results.png)

The results for the Vulnerability Reports and Hosts Scanning Reports should be available the next day if your select a ***daily*** scanning in the recipe.

  ![OCI VSS results hosts](./images/sec-vss-results-hosts.png)

 ![OCI VSS results hosts](./images/sec-vss-results-vulnerabilities.png)


### Task 2. Enable Cloud Guard (not available on Trial account)

1. Sign in to OCI tenancy; from the Oracle CLoud Console home page, click the **Navigation** Menu Button in the upper-left corner and hover over **Identity & Security** and select **Policies**.

    * Name: ***pol-cloudguard***
    * Description: ***Policy to allow Cloud Guard Service***
    * Compartment: ***root***
    * Policy Builder: ***Show Manual Editor***:

            allow service cloudguard to read vaults in tenancy
            allow service cloudguard to read keys in tenancy
            allow service cloudguard to read compartments in tenancy
            allow service cloudguard to read tenancies in tenancy
            allow service cloudguard to read audit-events in tenancy
            allow service cloudguard to read compute-management-family in tenancy
            allow service cloudguard to read instance-family in tenancy
            allow service cloudguard to read virtual-network-family in tenancy
            allow service cloudguard to read volume-family in tenancy
            allow service cloudguard to read database-family in tenancy
            allow service cloudguard to read object-family in tenancy
            allow service cloudguard to read load-balancers in tenancy
            allow service cloudguard to read users in tenancy
            allow service cloudguard to read groups in tenancy
            allow service cloudguard to read policies in tenancy
            allow service cloudguard to read dynamic-groups in tenancy
            allow service cloudguard to read authentication-policies in tenancy
            allow service cloudguard to use network-security-groups in tenancy


    * Click **Create**

    ![elbs-menu](./images/sec-cg-policy.png)

2. Click the **Navigation** Menu Button in the upper-left corner and hover over **Identity & Security** and select **Cloud Guard -> Overview**.

  ![OCI Cloud Guard](./images/cloudguard.png)

